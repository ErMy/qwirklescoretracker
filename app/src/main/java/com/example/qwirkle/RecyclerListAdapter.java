package com.example.qwirkle;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;

/**
 * @author Paul Burke (ipaulpro)
 */
public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private static final String[] STRINGS = new String[]{
            "Player1", "Player2", "Player3", "Player4"
    };

    private final List<String> mItems = new ArrayList<>();

    public RecyclerListAdapter() {
        mItems.addAll(Arrays.asList(STRINGS));
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        EditText name = ((EditText)holder.row.getChildAt(0));
        name.setText(mItems.get(position));
        name.setOnFocusChangeListener(new PlayerNameOnFocusChangeListener());
        name.setOnEditorActionListener(new DoneOnEditorActionListener());

        SharedPreferences sharedPref = name.getContext().getSharedPreferences("default", Context.MODE_PRIVATE);
        name.setText(sharedPref.getString("player" + Integer.toString(position), "Player" + Integer.toString(position+1)));

        ScoreEditText score = ((ScoreEditText)holder.row.getChildAt(1));
        score.setOnFocusChangeListener(new ScoreChangeOnFocusChangeListener());
        score.setOnEditorActionListener(new DoneOnEditorActionListener());

        switch (position)
        {
            case 0:
                name.setId(R.id.player0);
                score.setId(R.id.score0);
                break;
            case 1:
                name.setId(R.id.player1);
                score.setId(R.id.score1);
                break;
            case 2:
                name.setId(R.id.player2);
                score.setId(R.id.score2);
                break;
            case 3:
                name.setId(R.id.player3);
                score.setId(R.id.score3);
                break;
        }
    }

    @Override
    public void onItemDismiss(int position) {
//        mItems.remove(position);
//        notifyItemRemoved(position);
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        String prev = mItems.remove(fromPosition);
        mItems.add(toPosition > fromPosition ? toPosition - 1 : toPosition, prev);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final LinearLayout row;

        public ItemViewHolder(View itemView) {
            super(itemView);
            row = (LinearLayout) itemView;
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }
}
