package com.example.qwirkle;

import android.app.Application;

import java.util.Stack;

public class QwirkleApp extends Application
{
    public static Stack<Integer> undoStack;

    private static QwirkleApp singleton;

    public static QwirkleApp getInstance()
    {
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        undoStack = new Stack<Integer>();
    }
}
