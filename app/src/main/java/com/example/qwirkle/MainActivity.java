package com.example.qwirkle;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Stack;

public class MainActivity extends AppCompatActivity {

    RecyclerListFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fragment = (RecyclerListFragment) getSupportFragmentManager().findFragmentById(R.id.frag);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.reset_scores:
                resetScores();
                Toast.makeText(this, "Scores reset", Toast.LENGTH_SHORT).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void resetScores()
    {
        ((ScoreEditText) findViewById(R.id.score0)).reset();
        ((ScoreEditText) findViewById(R.id.score1)).reset();
        ((ScoreEditText) findViewById(R.id.score2)).reset();
        ((ScoreEditText) findViewById(R.id.score3)).reset();
    }

    @Override
    public void onBackPressed() {
        if (!Undo())
            super.onBackPressed();
    }

    public boolean Undo()
    {
        if (QwirkleApp.undoStack.empty())
            return false;
        else
        {
            int id = QwirkleApp.undoStack.pop();
            ScoreEditText score = (ScoreEditText) findViewById(id);
            score.undoLast();
        }
        return true;
    }
}

class DoneOnEditorActionListener implements TextView.OnEditorActionListener {
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            v.clearFocus();
            return true;
        }
        return false;
    }
}

class PlayerNameOnFocusChangeListener implements View.OnFocusChangeListener
{
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if(!hasFocus)
        {
            InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            SharedPreferences sharedPref = v.getContext().getSharedPreferences("default", Context.MODE_PRIVATE);
            switch (v.getId()){
                case R.id.player0:
                    sharedPref.edit().putString("player0", ((EditText)v).getText().toString()).apply();
                    break;
                case R.id.player1:
                    sharedPref.edit().putString("player1", ((EditText)v).getText().toString()).apply();
                    break;
                case R.id.player2:
                    sharedPref.edit().putString("player2", ((EditText)v).getText().toString()).apply();
                    break;
                case R.id.player3:
                    sharedPref.edit().putString("player3", ((EditText)v).getText().toString()).apply();
                    break;
            }


        }
    }
}

class ScoreChangeOnFocusChangeListener implements View.OnFocusChangeListener
{
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        ScoreEditText s = (ScoreEditText)v;
        InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

        if(!hasFocus)
        {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

            try
            {
                int increment = Integer.parseInt(s.getText().toString());
                s.increaseScore(increment);
            } catch (Exception e)
            {
            }
        }
        else {
            s.setText("");
            s.setHint(s.scoreStack.peek().toString() + " + ");

            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,0);
        }
    }
}