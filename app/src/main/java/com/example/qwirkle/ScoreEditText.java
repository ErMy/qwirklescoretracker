package com.example.qwirkle;
import android.content.Context;
import android.util.AttributeSet;

import java.util.Stack;

public class ScoreEditText extends androidx.appcompat.widget.AppCompatEditText
{
    public Stack<Integer> scoreStack;

    public ScoreEditText(Context context) {
        super(context);
        scoreStack = new Stack<Integer>();
        scoreStack.add(0);
    }

    public ScoreEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        scoreStack = new Stack<Integer>();
        scoreStack.add(0);
    }

    public ScoreEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        scoreStack = new Stack<Integer>();
        scoreStack.add(0);
    }

    public void increaseScore(int increment)
    {
        int newScore = increment + scoreStack.peek();
        scoreStack.add(newScore);
        QwirkleApp.undoStack.add(getId());
        setText(Integer.toString(newScore));
    }

    public void reset()
    {
        scoreStack.clear();
        scoreStack.add(0);
        setText("0");
    }

    public void undoLast()
    {
        scoreStack.pop();
        setText(scoreStack.peek().toString());
    }
}
