package com.example.qwirkle;


public interface ItemTouchHelperViewHolder {
    void onItemSelected();

    void onItemClear();
}
